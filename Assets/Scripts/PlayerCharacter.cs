﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCharacter : MonoBehaviour {

    private Vector2 positionInTerrain;

    public float characterSpeed;

    public bool isMoving, doingMovement;
    private Vector3 movementDestination, movementInitialPosition;
    private Vector2 mapDestination;
    private float movementStartTime;
    private float movementLenght;
    private float worldTimeOfMovement;
    private IslandGenerator islandGenerator;
    private GameMaster gameMaster;

    private List<Vector2> movementsToDo;

    // Use this for initialization
    void Start () {
        isMoving = false;
        doingMovement = false;
        islandGenerator = GameObject.FindObjectOfType<IslandGenerator>();
        gameMaster = GameObject.FindObjectOfType<GameMaster>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (isMoving)
        {            
            if (doingMovement)
            {
                float movementDone = (Time.time - movementStartTime) * characterSpeed; //TODO usar delta time para igualar movimiento
                float movementFraction = movementDone / movementLenght;
                transform.position = Vector3.Lerp(movementInitialPosition, movementDestination, movementFraction);
                if (transform.position == movementDestination)
                {
                    doingMovement = false;
                    positionInTerrain = mapDestination;
                    CheckTerrainPosition();
                    gameMaster.TimePasses(worldTimeOfMovement);
                }
            } else
            {
                if (movementsToDo.Count > 0)
                    NextMovement();
                else
                    isMoving = false;
            }
        }
	}

    public void MoveTo(float x, float y, int terrainX, int terrainY)
    {
        if (!isMoving)
        {
            mapDestination = new Vector2(terrainX, terrainY);
            movementsToDo = Pathfinding.BestPath(positionInTerrain, mapDestination, islandGenerator.terrain, islandGenerator.roadConnections);
            if (movementsToDo.Count > 0)
                isMoving = true;
        }
    }

    private void NextMovement()
    {
        mapDestination = movementsToDo[0];
        movementsToDo.RemoveAt(0);
        movementInitialPosition = transform.position;
        movementDestination = new Vector3(
            islandGenerator.terrain[(int)mapDestination.x, (int)mapDestination.y].worldPosition.x,
            movementInitialPosition.y,
            islandGenerator.terrain[(int)mapDestination.x, (int)mapDestination.y].worldPosition.y
            );
        movementStartTime = Time.time;
        movementLenght = Vector3.Distance(movementInitialPosition, movementDestination);
        worldTimeOfMovement = (islandGenerator.terrain[(int)mapDestination.x, (int)mapDestination.y].GetTimeCost() + islandGenerator.terrain[(int)positionInTerrain.x, (int)positionInTerrain.y].GetTimeCost()) / 2;
        if (islandGenerator.roadConnections[(int)positionInTerrain.x, (int)positionInTerrain.y, (int)mapDestination.x, (int)mapDestination.y])
            worldTimeOfMovement /= 3;
        doingMovement = true;
    }

    public void SetInitialPosition(int x, int y)
    {
        positionInTerrain = new Vector2(x, y);
        CheckTerrainPosition();
    }

    private void CheckTerrainPosition()
    {
        if (GameObject.FindObjectOfType<IslandGenerator>().terrain[(int)positionInTerrain.x, (int)positionInTerrain.y].featureType != FeatureType.None)
        {
            gameObject.GetComponent<CharacterPawn>().SetBaseVisible(false);
        } else
        {
            gameObject.GetComponent<CharacterPawn>().SetBaseVisible(true);
        }
    }

}
