﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(MapViewTile))]
public class MapViewTileEditor : Editor {

    public override void OnInspectorGUI()
    {
        MapViewTile myMapTile = (MapViewTile)target;

        DrawDefaultInspector();

        EditorGUILayout.LabelField("Bitmask tile", myMapTile.mapTile.bitmaskType.ToString());

        if (GUILayout.Button("Set Bitmask"))
        {
            myMapTile.SetTileByMask();
        }
    }

    
}
