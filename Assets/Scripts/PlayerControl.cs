﻿using UnityEngine;
using System.Collections;

/**
    PlayerControl

    Input controls of the player
*/
public class PlayerControl : MonoBehaviour {

    //main camera of the game
    public Camera playerCamera;

    private PlayerCharacter pc;

    //speed of the camera
    public float cameraSpeed;

    //speed of the camera zoom
    public float cameraZoomSpeed;

    //speed of the camera rotation
    public float cameraRotationSpeed;

    //reference to the IslandGenerator
    private IslandGenerator islandGenerator;

    //shortcut to playerCamera.transform
    private Transform cameraTransform;

    //Size of a tile, in world units
    private Vector2 tileSize;

    //Size of the map, in world units
    private Vector2 mapSize;

    //Maximum and minimum position of the camera, in (x,y), in world units
    private float cameraMinX, cameraMinY, cameraMaxX, cameraMaxY;

    /**
    inits variables
    calculates max and min positions of camera
    */
    void Start () {
        //Init islandGenerator
        islandGenerator = GameObject.FindObjectOfType<IslandGenerator>();

        pc = GameObject.FindObjectOfType<PlayerCharacter>();

        //Get sizes
        mapSize = islandGenerator.GetMapSize();
        tileSize = islandGenerator.GetTileSize();

        //Init camera transform
        cameraTransform = playerCamera.transform;

        //Calculate max and min camera positions
        cameraMinX = -1 * playerCamera.orthographicSize * playerCamera.aspect; //Half of the camera width
        cameraMinY = -1 * playerCamera.orthographicSize;  //Half of the camera height
        cameraMaxX = mapSize.x - cameraMinX - tileSize.x / 2;  //Size of the map, minus half camera width, minus half a tile
        cameraMaxY = mapSize.y - cameraMinY - 1.5f * tileSize.y;  //Size of the map, minus half camera height, minus tile and a half
    }
		
    /**
    Gets player input and acts as needed
    */
	void Update ()
    {
        //Camera movement
        UpdateCameraPosition();

        //Camera Zoom
        UpdateCameraZoom();

        //Camera Rotation
        UpdateCameraRotation();
    }

    private void UpdateCameraPosition()
    {
        float xAxis, yAxis;

        if (Input.GetButton("CameraMovement"))
        {
            xAxis = Input.GetAxis("Mouse X");
            yAxis = Input.GetAxis("Mouse Y");
        } else
        {
            xAxis = Input.GetAxis("Horizontal");
            yAxis = Input.GetAxis("Vertical");
        }

        Vector3 cameraPos = cameraTransform.position;
        float newX = cameraPos.x + xAxis * cameraSpeed;
        float newY = cameraPos.y;
        float newZ = cameraPos.z + yAxis * cameraSpeed;

        //Check limits
        if (newX < cameraMinX)
            newX = cameraMinX;
        if (newX > cameraMaxX)
            newX = cameraMaxX;
        if (newZ < cameraMinY)
            newZ = cameraMinY;
        if (newZ > cameraMaxY)
            newZ = cameraMaxY;

        //Update camera position
        cameraTransform.position = new Vector3(newX, newY, newZ);
    }

    private void UpdateCameraZoom()
    {
        playerCamera.fieldOfView += Input.GetAxis("Zoom") * cameraZoomSpeed;
    }

    private void UpdateCameraRotation()
    {
        if (Input.GetButton("CameraRotation"))
        {
            cameraTransform.Rotate(new Vector3(-1 * Input.GetAxis("Mouse Y") * cameraRotationSpeed, 0, Input.GetAxis("Mouse X") * cameraRotationSpeed));
        }
    }

    public void MoveHero(float x, float y, int terrainX, int terrainY)
    {
        pc.MoveTo(x, y, terrainX, terrainY);        
    }

}
