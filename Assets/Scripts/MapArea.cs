﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MapArea : MonoBehaviour {

    public string areaName = "";

    public TileType tileType;

    private List<MapTile> areaTiles;

    private float centerX,centerY;

    private Text text;

	// Use this for initialization
	void Awake () {
        areaTiles = new List<MapTile>();

        text = GetComponentInChildren<Text>();

        GetComponentInChildren<Canvas>().worldCamera = GameObject.FindObjectOfType<Camera>();
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    public void SetAreaName(string areaName)
    {
        this.areaName = areaName;
        text.text = areaName;
    }

    public void addAreaTile(MapTile newAreaTile)
    {
        areaTiles.Add(newAreaTile);

        float totalX = 0, totalY = 0;
        int count = 0;

        foreach (MapTile mapTile in areaTiles)
        {
            totalX += mapTile.worldPosition.x;
            totalY += mapTile.worldPosition.y;
            count++;
        }

        centerX = totalX / count;
        centerY = totalY / count;

        gameObject.transform.position = new Vector3(centerX, gameObject.transform.position.y, centerY);
    }
}
