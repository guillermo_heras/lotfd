﻿using UnityEngine;
using System.Collections;

public class MapViewTile : MonoBehaviour
{

    public MapTile mapTile;

    public GameObject[] bm = new GameObject[14];

    public GameObject[] trees;

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject.FindObjectOfType<PlayerControl>().MoveHero(mapTile.worldPosition.x, mapTile.worldPosition.y, (int)mapTile.gridPosition.x, (int)mapTile.gridPosition.y);
        }
    }

    public void SetTileByMask()
    {
        if ((mapTile.tileType != TileType.DeepWater) && (mapTile.tileType != TileType.ShallowWater))
        {
            for (int i = 0; i < bm.Length; i++)
            {
                bm[i].SetActive(false);
            }
            bm[mapTile.bitmaskType].SetActive(true);
            Vector3 rotation = bm[mapTile.bitmaskType].transform.rotation.eulerAngles;
            rotation = new Vector3(rotation.x, 180+mapTile.bitmaskAngle, rotation.z);
            bm[mapTile.bitmaskType].transform.rotation = Quaternion.Euler(rotation);
        }
    }
}

