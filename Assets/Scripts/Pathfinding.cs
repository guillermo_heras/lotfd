﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class PFNode
{
    public int X;
    public int Y;

    public PFNode(Vector2 pos)
    {
        X = (int)pos.x;
        Y = (int)pos.y;
    }

    override public string  ToString()
    {
        return X.ToString() + "/" + Y.ToString();
    }

    public bool Equals(PFNode obj)
    {
        return ((X == (int)obj.X) && (Y == (int)obj.Y));
    }

    public bool Equals(Vector2 obj)
    {
        return ((X == (int)obj.x) && (Y == (int)obj.y));
    }

    public Vector2 ToVector2()
    {
        return new Vector2((int)X, (int)Y);
    }
}

public static class Pathfinding {

    public static List<Vector2> BestPath(Vector2 origin, Vector2 destination, MapTile[,] map, bool[,,,] roadConnections)
    {
        Dictionary<string, PFNode> open = new Dictionary<string, PFNode>();
        Dictionary<string, PFNode> closed = new Dictionary<string, PFNode>();
        Dictionary<string, float> gScore = new Dictionary<string, float>();
        Dictionary<string, float> fScore = new Dictionary<string, float>();
        Dictionary<string, PFNode> cameFrom = new Dictionary<string, PFNode>();

        PFNode startNode = new PFNode(origin);

        open.Add(startNode.ToString(),startNode);
        gScore[startNode.ToString()] = 0;
        fScore[startNode.ToString()] = CalculateCostToDestination(startNode.ToVector2(), destination);

        while (open.Count > 0)
        {
            PFNode currentNode = SelectNode(origin, destination, map, ref open, ref closed, ref fScore);

            if (currentNode.Equals(destination))
            {
                return ReconstructPath(cameFrom, currentNode);
            }

            open.Remove(currentNode.ToString());
            closed.Add(currentNode.ToString(), currentNode);

            int x, y;
            int m = currentNode.X % 2;
            //Up node
            x = currentNode.X;
            y = currentNode.Y + 1;
            CheckNeighbour(destination, map, ref open, ref closed, ref cameFrom, ref fScore, ref gScore, currentNode, x, y, roadConnections);

            //Up-Right node
            x = currentNode.X + 1;
            y = currentNode.Y + 1 - m;
            CheckNeighbour(destination, map, ref open, ref closed, ref cameFrom, ref fScore, ref gScore, currentNode, x, y, roadConnections);

            //Down Right node
            x = currentNode.X + 1;
            y = currentNode.Y - m;
            CheckNeighbour(destination, map, ref open, ref closed, ref cameFrom, ref fScore, ref gScore, currentNode, x, y, roadConnections);

            //Down node
            x = currentNode.X;
            y = currentNode.Y - 1;
            CheckNeighbour(destination, map, ref open, ref closed, ref cameFrom, ref fScore, ref gScore, currentNode, x, y, roadConnections);

            //Down Left node
            x = currentNode.X - 1;
            y = currentNode.Y - m;
            CheckNeighbour(destination, map, ref open, ref closed, ref cameFrom, ref fScore, ref gScore, currentNode, x, y, roadConnections);

            //Up Left node
            x = currentNode.X - 1;
            y = currentNode.Y + 1 - m;
            CheckNeighbour(destination, map, ref open, ref closed, ref cameFrom, ref fScore, ref gScore, currentNode, x, y,roadConnections);
        }

        //No solution
        List<Vector2> solution = new List<Vector2>();
        return solution;
    }

    private static void CheckNeighbour(Vector2 destination, MapTile[,] map, ref Dictionary<string, PFNode> open, 
        ref Dictionary<string, PFNode> closed, ref Dictionary<string, PFNode> cameFrom,
        ref Dictionary<string, float> fScore, ref Dictionary<string, float> gScore,
        PFNode currentNode, int x, int y,
        bool[,,,] roadConnections)
    {
        if (CheckCoordinates(x, y, map))
        {
            PFNode neighborNode = GetNode(x, y);
            if (!closed.ContainsKey(neighborNode.ToString()))
            {
                float tentative_gScore = gScore[currentNode.ToString()] + DistanceBetweenNeighbourTiles(currentNode, neighborNode, map, roadConnections);

                if (!open.ContainsKey(neighborNode.ToString()))
                {
                    open.Add(neighborNode.ToString(), neighborNode);
                }
                else if (tentative_gScore >= gScore[neighborNode.ToString()])
                    return;

                if (cameFrom.ContainsKey(neighborNode.ToString()))
                {
                    cameFrom.Remove(neighborNode.ToString());
                }
                cameFrom.Add(neighborNode.ToString(), currentNode);
                gScore[neighborNode.ToString()] = tentative_gScore;
                fScore[neighborNode.ToString()] = gScore[neighborNode.ToString()] + CalculateCostToDestination(neighborNode.ToVector2(), destination);
            }
        }
    }

    private static float DistanceBetweenNeighbourTiles(PFNode node1, PFNode node2, MapTile[,] map, bool[,,,] roadConnections)
    {
        int divisor = roadConnections[node1.X, node1.Y, node2.X, node2.Y] ? 6 : 2;
        return (map[node1.X, node1.Y].GetTimeCost() + map[node2.X, node2.Y].GetTimeCost()) / divisor;
    }

    private static List<Vector2> ReconstructPath(Dictionary<string, PFNode> cameFrom, PFNode current)
    {
        List<Vector2> invertedPath = new List<Vector2>();
        

        invertedPath.Add(current.ToVector2());

        while (cameFrom.ContainsKey(current.ToString()))
        {
            current = cameFrom[current.ToString()];
            invertedPath.Add(current.ToVector2());
        }

        List<Vector2> path = new List<Vector2>();
        for (int x = 1; x < invertedPath.Count; x++)
        {
            path.Add(invertedPath[invertedPath.Count - 1 - x]);
        }
        return path;
    }

    private static PFNode SelectNode(Vector2 origin, Vector2 destination, MapTile[,] map, 
        ref Dictionary<string, PFNode> open, ref Dictionary<string, PFNode> closed, ref Dictionary<string, float> fScore)
    {
        PFNode selectedNode = open.Values.First<PFNode>();
        float best = fScore[selectedNode.ToString()];
        foreach (KeyValuePair<string, PFNode> keyNode in open)
        {
            PFNode node = keyNode.Value;
            if (fScore[node.ToString()] < best)
            {
                best = fScore[node.ToString()];
                selectedNode = node;
            }
        }
        return selectedNode;
    }

    private static PFNode GetNode(int x, int y)
    {
        return new PFNode(new Vector2(x, y));
    }


    private static bool CheckCoordinates(int x, int y, MapTile[,] map)
    {
        return (
            (x >= 0) && (y >= 0) && 
            (x < map.GetLength(0)) && (y < map.GetLength(1)) &&
            map[x,y].tileType != TileType.DeepWater
            );
    }

    private static int CalculateCostToDestination(Vector2 pos, Vector2 destination)
    {
        return 3 * (int)(Mathf.Abs(destination.x - pos.x) + Mathf.Abs(destination.y - pos.y));
    }
}
