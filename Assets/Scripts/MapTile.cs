﻿using UnityEngine;
using System.Collections;

public enum TileType {Grass, Hills, Mountains, Desert, Forest, ShallowWater, DeepWater};

public enum FeatureType { None, Town, City, Lair, Special };

[System.Serializable]
public class MapTile
{

    public Vector2 gridPosition;

    public TileType tileType;

    public FeatureType featureType;

    public Vector2 worldPosition;

    public GameObject worldTile;

    public GameObject featureTile;

    public MapArea mapArea;

    public string areaName;

    public string featureName;

    [Range(0,63)]
    public int bitmask;

    public int bitmaskType
    {
        get
        {
            switch (bitmask)
            {
                case 0:
                    return 0;
                case 1:
                case 2:
                case 4:
                case 8:
                case 16:
                case 32:
                    return 1;
                case 3:
                case 6:
                case 12:
                case 24:
                case 33:
                case 48:
                    return 2;
                case 5:
                case 10:
                case 17:
                case 20:
                case 34:
                case 40:
                    return 3;
                case 7:
                case 14:
                case 28:
                case 35:
                case 49:
                case 56:
                    return 4;
                case 9:
                case 18:
                case 36:
                    return 5;
                case 11:
                case 22:
                case 25:
                case 37:
                case 44:
                case 50:
                    return 6;
                case 13:
                case 19:
                case 26:
                case 38:
                case 41:
                case 52:
                    return 7;
                case 15:
                case 30:
                case 39:
                case 51:
                case 57:
                case 60:
                    return 8;
                case 21:
                case 42:
                    return 9;
                case 23:
                case 29:
                case 43:
                case 46:
                case 53:
                case 58:
                    return 10;
                case 27:
                case 45:
                case 54:
                    return 11;
                case 31:
                case 47:
                case 55:
                case 59:
                case 61:
                case 62:
                    return 12;
                default:
                    return 13;
            }
        }
    }

    public int bitmaskAngle
    {
        get
        {
            switch (bitmask)
            {
                case 0:
                case 1:
                case 3:
                case 5:
                case 7:
                case 9:
                case 11:
                case 13:
                case 15:
                case 21:
                case 23:
                case 27:
                case 31:
                case 63:
                    return 0;                
                case 2:
                case 6:
                case 10:
                case 14:
                case 18:
                case 22:
                case 26:
                case 28:
                case 30:
                case 42:
                case 46:
                case 54:
                case 62:
                    return 60;
                case 4:
                case 12:
                case 20:
                case 29:
                case 44:
                case 45:
                case 52:
                case 56:
                case 60:
                case 61:
                    return 120;
                case 8:
                case 24:
                case 25:
                case 40:
                case 41:
                case 49:
                case 57:
                case 58:
                case 59:
                    return 180;
                case 16:
                case 17:
                case 19:
                case 35:
                case 48:
                case 50:
                case 51:
                case 53:
                case 55:
                    return 240;
                case 32:
                case 33:
                case 34:
                case 36:
                case 37:
                case 38:
                case 39:
                case 43:
                case 47:
                    return 300;
                default:
                    return 0;
            }
        }
    }

    public MapTile(int x, int y)
    {
        gridPosition.x = x;
        gridPosition.y = y;
    }

    public int GetTimeCost()
    {
        switch (tileType)
        {
            case TileType.Grass :
                return 3;
            case TileType.Hills :
                return 5;
            case TileType.Forest :
                return 6;
            case TileType.Mountains:
                return 8;
            case TileType.Desert:
                return 10;
            case TileType.ShallowWater:
                return 14;
            default:
                return 7;
        }
    }
}

