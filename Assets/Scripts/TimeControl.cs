﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeControl : MonoBehaviour {

    public int initialHour = 10;
    public int initialDay = 1;
    public int initialMonth = 1;
    public int initialYear = 327;

    public int monthsPerYear = 16;
    public int[] daysPerMonth;
    public int hoursPerDay = 24;

    public Text HourText, DayText, MonthText, YearText;

    public Light sunLight,moonLight;

    private int year;
    private int month;
    private int day;
    private int hour;

    private float fractionalHours;

    // Use this for initialization
    void Start()
    {
        fractionalHours = initialHour;
        hour = initialHour;
        year = initialYear;
        month = initialMonth;
        day = initialDay;
        AdjustTime();
        AdjustLight();
        UpdateUI();
    }

    void UpdateUI()
    {
        HourText.text = hour.ToString();
        DayText.text = day.ToString();
        MonthText.text = month.ToString();
        YearText.text = year.ToString();
    }

    int DaysInMonth(int month)
    {
        return daysPerMonth[month - 1];
    }

    private void AdjustTime()
    {
        day += hour / hoursPerDay;
        hour = hour % hoursPerDay;

        while (day > DaysInMonth(month))
        {
            day -= DaysInMonth(month);
            month++;
            if (month > monthsPerYear)
            {
                month = 1;
                year++;
            }
        }
    }

    private void AdjustLight()
    {
        float partOfDay = (float)((float)hour / (float)hoursPerDay);

        Vector3 rotationVector = sunLight.transform.rotation.eulerAngles;
        rotationVector = new Vector3(Mathf.Lerp(0, 360, partOfDay),rotationVector.y, rotationVector.z);

        sunLight.transform.rotation = Quaternion.Euler(rotationVector);

        if ((partOfDay  <= 0.25f) || (partOfDay > 0.75f))
        {
            moonLight.intensity = 0.4f;
        } else
        {
            moonLight.intensity = 0f;
        }

    }

    public void TimeJump(float hours)
    {
        int fullHours;
        fractionalHours += hours;
        fullHours = Mathf.FloorToInt(fractionalHours);
        this.hour += fullHours; 
        fractionalHours -= fullHours;
        AdjustTime();
        AdjustLight();
        UpdateUI();
    }

    public void TimeJump(float hours, int days)
    {
        TimeJump(hours + days * hoursPerDay);
    }

}
