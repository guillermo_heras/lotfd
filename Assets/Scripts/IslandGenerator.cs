﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct TileDataForMap
{
    public string nameOfTile;

    public TileType tileType;

    public GameObject tile;

    public float chance;
}

[System.Serializable]
public struct FeatureDataForMap
{
    public string nameOfFeature;

    public FeatureType feature;

    public GameObject tile;

    public float chance;

    public int minNumber;

    public int maxNumber;

    public bool canBeInWater;

}

/**
    IslandGenerator

    Main class to generate the map world
**/
public class IslandGenerator : MonoBehaviour {

    //These variables hold the different tiles in the world
    public TileDataForMap[] tileData;

    //This dictionary will hold all the previous tiles, indexed by tile type
    private Dictionary<TileType, TileDataForMap> terrainTiles;

    //These variables hold the different features in the world
    public FeatureDataForMap[] featureData;

    //This dictionary will hold all the previous features, indexed by feature type
    private Dictionary<FeatureType, FeatureDataForMap> featureTiles;

    public GameObject roadSegment;

    public bool[,,,] roadConnections;

    //The GameObject that will contain the tiles generated
    public GameObject islandMap;

    public GameObject mapArea;

    public List<GameObject> areaNames;

    //Between 0 and 100, indicates how much will the sea water advance
    public float seaSize;

    public int mountainRangeLength;

    public float mountainRangeChance;

    public float hillsNextToMountainsChance;

    //Random Seed to use to generate the map
    public int randomSeed;
    
    //Terrain width and height, in number of tiles
    public int terrainWidth, terrainHeight;

    //Width and height of a tile, in world units
    private float tw, th;

    private MonsterController monsterController;

    public GameObject roadContainer, mapAreasContainer;

    //will contain the island definition
    public MapTile[,] terrain; 

    public Dictionary<FeatureType, List<MapTile>> featuresInMap;

    /**
    Put tiles in list
    Calculate tile size
    Init terrain array
    */
    void Awake()
    {
        //Add tiles to the dictionary of tiles
        terrainTiles = new Dictionary<TileType, TileDataForMap>();
        for (int i = 0; i < tileData.Length; i++)
            terrainTiles.Add(tileData[i].tileType, tileData[i]);

        featureTiles = new Dictionary<FeatureType, FeatureDataForMap>();
        for (int i = 0; i < featureData.Length; i++)
            featureTiles.Add(featureData[i].feature, featureData[i]);

        featuresInMap = new Dictionary<FeatureType, List<MapTile>>();
        foreach (FeatureType ft in System.Enum.GetValues(typeof(FeatureType)))
            featuresInMap.Add(ft, new List<MapTile>());

        //Calculate size of a tile
        tw = tileData[0].tile.GetComponentInChildren<MeshRenderer>().bounds.size.x;
        th = tileData[0].tile.GetComponentInChildren<MeshRenderer>().bounds.size.z;

        //Init terrain array
        terrain = new MapTile[terrainWidth, terrainHeight];

        roadConnections = new bool[terrainWidth, terrainHeight, terrainWidth, terrainHeight];

        areaNames = new List<GameObject>();

        monsterController = gameObject.GetComponent<MonsterController>();
    }

    /**
    Generate terrain
    */
    public void GenerateTerrain () {
        //Init random seed
        //TODO activate random seed
        //Random.seed = randomSeed;

        InitiateTerrain();

        SeaErosion();

        SmoothTerrain();

        MakeMountainRanges();

        AddShallowWater();

        AddFeatures();

        CalculateBitmasks();

        InstantiateTerrain();

        InstantiateMonsters();

        PutRoads();

        NameTerrainAreas();
	}

    /**
    Returns a Vector2 with the size of the world, in world units
    */
    public Vector2 GetMapSize()
    {
        return new Vector2(
            tw * 0.75f * terrainWidth,   //In a hex grid, there is only 3/4 of the tiles horizontally
            th * (0.5f + terrainHeight)  //In a hex grid, there is half an extra tile vertically
            );
    }

    /**
    Returns the size of a tile, in world units
    */
    public Vector2 GetTileSize()
    {
        return new Vector2(tw, th);
    }

    /**
    Generates the world map randomly
    */
    void InitiateTerrain()
    {
        //To contain the type of tile
        TileType tile;

        //To contain each new space created
        MapTile newTile;

        float posX, posY;

        //Main loop that will go through each cell in the hex grid, from (0,0) to (terrainWidth-1,terrainHeight-1)
        for (int x = 0; x < terrainWidth; x++)
        {
            for (int y = 0; y < terrainHeight; y++)
            {
                //This will keep a border of two deep water tiles around the whole island
                if ((x >= terrainWidth-2) || (x <= 1) ||(y >= terrainHeight-2) || (y<=1))
                {
                    tile = TileType.DeepWater;
                } else
                {
                    tile = GetRandomTerrainTile();
                }

                //Depending on the coordinates, the calculation of the world position Y of the tile will vary
                posX = x * 0.75f * tw;
                if (x % 2 == 0)
                {
                    posY = y * th;
                } else
                {
                    posY = (-0.5f + y) * th;
                }
                newTile = new MapTile(x,y);

                newTile.tileType = tile;

                newTile.featureType = FeatureType.None;

                newTile.worldPosition = new Vector2(posX, posY);

                terrain[x, y] = newTile;

            }
        }
    }

    void SeaErosion()
    {
        int x, y;

        for (int seaLoop = 0; seaLoop <= ((terrainHeight < terrainWidth) ? terrainHeight / 2 : terrainWidth / 2); seaLoop++)
        {
            x = seaLoop;
            for (y = seaLoop; y < terrainHeight-seaLoop; y++)
            {
                CheckSeaErosion(x, y);
            }

            y = seaLoop;
            for (x = seaLoop+1; x < terrainWidth - seaLoop; x++)
            {
                CheckSeaErosion(x, y);
            }

            y = terrainHeight - 1 - seaLoop;
            for (x = seaLoop + 1; x < terrainWidth - seaLoop; x++)
            {
                CheckSeaErosion(x, y);
            }

            x = terrainWidth - 1 - seaLoop;
            for (y = seaLoop + 1; y < terrainHeight - 1 - seaLoop; y++)
            {
                CheckSeaErosion(x, y);
            }
        }
    }

    private void CheckSeaErosion(int x, int y)
    {
        if (terrain[x, y].tileType != TileType.DeepWater)
        {
            int neighbourDeepWaterTiles = GetSurroundingTiles(x, y, TileType.DeepWater);
            if (neighbourDeepWaterTiles >= 1)
            {
                if (Random.Range(0, 100) <= neighbourDeepWaterTiles * seaSize)
                    terrain[x, y].tileType = TileType.DeepWater;
            }
        }
    }

    void SmoothTerrain()
    {
        //Main loop that will go through each cell in the hex grid, from (0,0) to (terrainWidth-1,terrainHeight-1)
        for (int x = 2; x < terrainWidth-2; x++)
        {
            for (int y = 2; y < terrainHeight-2; y++)
            {
                int neighbourGrassTiles = GetSurroundingTiles(x, y, TileType.Grass);
                int neighbourHillsTiles = GetSurroundingTiles(x, y, TileType.Hills);
                int neighbourMountainsTiles = GetSurroundingTiles(x, y, TileType.Mountains);
                int neighbourDesertTiles = GetSurroundingTiles(x, y, TileType.Desert);
                int neighbourForestTiles = GetSurroundingTiles(x, y, TileType.Forest);
                int neighbourShallowWaterTiles = GetSurroundingTiles(x, y, TileType.ShallowWater);                

                if (neighbourGrassTiles >= 5)
                {
                    terrain[x, y].tileType = TileType.Grass;
                }
                else if (neighbourHillsTiles >= 4)
                {
                    terrain[x, y].tileType = TileType.Hills;
                }
                else if (neighbourMountainsTiles >= 4)
                {
                    terrain[x, y].tileType = TileType.Mountains;
                }
                else if (neighbourDesertTiles >= 4)
                {
                    terrain[x, y].tileType = TileType.Desert;
                }
                else if (neighbourForestTiles >= 4)
                {
                    terrain[x, y].tileType = TileType.Forest;
                }
                else if (neighbourShallowWaterTiles >= 4)
                {
                    terrain[x, y].tileType = TileType.ShallowWater;
                }
            }
        }
    }

    void MakeMountainRanges()
    {
        for (int x = 0; x < terrainWidth; x++)
        {
            for (int y = 0; y < terrainHeight; y++)
            {
                if (terrain[x, y].tileType == TileType.Mountains)
                {
                    for (int rangeLength = mountainRangeLength; rangeLength > 2; rangeLength--)
                    {
                        int i, j;
                        i = x - rangeLength;
                        if (i >= 0)
                        {
                            for (j = y - rangeLength; j <= y + rangeLength; j++)
                            {
                                if ((j >= 0) && (j < terrainHeight))
                                {
                                    if (terrain[i, j].tileType == TileType.Mountains)
                                    {
                                        checkPossibleMountainRange(x, y, i, j);
                                    }
                                }
                            }
                        }

                        j = y - rangeLength;
                        if (j >= 0)
                        {
                            for (i = x - rangeLength + 1; i < x + rangeLength; i++)
                            {
                                if ((i < terrainWidth) && (i >= 0))
                                {
                                    if (terrain[i, j].tileType == TileType.Mountains)
                                    {
                                        checkPossibleMountainRange(x, y, i, j);
                                    }
                                }
                            }
                        }

                        j = y + rangeLength;
                        if (j < terrainHeight)
                        {
                            for (i = x -rangeLength + 1; i < x + rangeLength; i++)
                            {
                                if ((i < terrainWidth) && (i >=0))
                                {
                                    if (terrain[i, j].tileType == TileType.Mountains)
                                    {
                                        checkPossibleMountainRange(x, y, i, j);
                                    }
                                }
                            }
                        }

                        i = x + rangeLength;
                        if (i < terrainWidth)
                        {
                            for (j = y - rangeLength + 1; j < y + rangeLength - 1; j++)
                            {
                                if ((j >= 0) && (j < terrainHeight))
                                {
                                    if (terrain[i, j].tileType == TileType.Mountains)
                                    {
                                        checkPossibleMountainRange(x, y, i, j);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //Loop to put hills around mountains
        for (int x = 0; x < terrainWidth; x++)
        {
            for (int y = 0; y < terrainHeight; y++)
            {
                if (terrain[x, y].tileType == TileType.Mountains)
                {
                    for (int i = x - 1; i <= x + 1; i++)
                    {
                        for (int j = y - 1; j <= y + 1; j++)
                        {
                            if ((terrain[i, j].tileType != TileType.Mountains) && (terrain[i, j].tileType != TileType.DeepWater) && (terrain[i, j].tileType != TileType.ShallowWater))
                            {
                                if (Random.value <= hillsNextToMountainsChance)
                                    terrain[i, j].tileType = TileType.Hills;
                            }
                        }
                    }
                }
            }
        }
    }

    void checkPossibleMountainRange(int startX, int startY, int endX, int endY)
    {
        if (Random.Range(0,100) <= mountainRangeChance)
        {
            int lastX = startX;
            int lastY = startY;

            while ((lastX != endX) || (lastY != endY)) {
                bool changeX = true;

                if (lastX == endX) {
                    changeX = false;
                }  else if (lastY != endY)
                {
                    changeX = (Random.value <= 0.5f);
                } else
                {
                    changeX = true;
                }

                if (changeX)
                {
                    if (endX > lastX)
                        lastX++;
                    else
                        lastX--;
                } else
                {
                    if (endY > lastY)
                        lastY++;
                    else
                        lastY--;
                }

                if ((terrain[lastX,lastY].tileType != TileType.ShallowWater) && (terrain[lastX, lastY].tileType != TileType.DeepWater))
                {
                    terrain[lastX, lastY].tileType = TileType.Mountains;
                }
            }
        }
    }

    void AddShallowWater()
    {
        //Main loop that will go through each cell in the hex grid, from (0,0) to (terrainWidth-1,terrainHeight-1)
        for (int x = 0; x < terrainWidth; x++)
        {
            for (int y = 0; y < terrainHeight; y++)
            {
                if (terrain[x, y].tileType == TileType.DeepWater)
                {
                    if ((GetSurroundingTiles(x, y, TileType.Grass) >0) || (GetSurroundingTiles(x, y, TileType.Hills) > 0) || (GetSurroundingTiles(x, y, TileType.Desert) > 0) || (GetSurroundingTiles(x, y, TileType.Forest) >0))
                    {
                        terrain[x, y].tileType = TileType.ShallowWater;
                    }
                    
                }
            }
        }
    }

    void AddFeatures()
    {
        foreach (FeatureDataForMap feature in featureData)
        {
            int count = 0;

            int numFeatures = Random.Range(feature.minNumber, feature.maxNumber);
            while (count < numFeatures)
            {
                int x = Random.Range(0, terrainWidth - 1);
                int y = Random.Range(0, terrainHeight - 1);

                if ((feature.canBeInWater) || ((terrain[x,y].tileType != TileType.ShallowWater) && (terrain[x, y].tileType != TileType.DeepWater)))
                {
                    terrain[x, y].featureType = feature.feature;
                    count++;
                    featuresInMap[feature.feature].Add(terrain[x, y]);

                    if (feature.feature == FeatureType.Lair)
                    {
                        GameObject monster = monsterController.getRandomMonster();
                        monsterController.AddMonsterToMap(monster, x, y);
                    }

                }
            }
        }
    }

    void CalculateBitmasks()
    {
        for (int x = 0; x < terrainWidth;x++)
        {
            for (int y = 0; y < terrainHeight; y++)
            {
                SetBitmask(x, y);
            }
        }
    }

    void SetBitmask(int x, int y)
    {
        int m = x % 2;

        terrain[x, y].bitmask = IsLand(x, y + 1) + IsLand(x + 1, y + 1 - m) * 2 + IsLand(x + 1, y - m) * 4 + IsLand(x, y - 1) * 8 + IsLand(x - 1, y - m) * 16 + IsLand(x - 1, y + 1 - m) * 32;
    }

    int IsLand(int x, int y)
    {
        if ((x < 0) || (y < 0) || (x >= terrainWidth) || (y >= terrainHeight))
            return 0;

        if ((terrain[x, y].tileType == TileType.DeepWater) || (terrain[x, y].tileType == TileType.ShallowWater))
            return 0;
        else
            return 1;
    }

    void InstantiateTerrain()
    {
        GameObject newTile;
        GameObject newFeature;
        float yValue;

        //Main loop that will go through each cell in the hex grid, from (0,0) to (terrainWidth-1,terrainHeight-1)
        for (int x = 0; x < terrainWidth; x++)
        {
            for (int y = 0; y < terrainHeight; y++)
            {
                //Add as a child to the map
                if (terrain[x, y].tileType == TileType.DeepWater)
                {
                    yValue = -0.7f;
                }
                else if (terrain[x, y].tileType == TileType.ShallowWater)
                {
                    yValue = -0.3f;
                } else
                {
                    yValue = 0.1f;
                }

                newTile = (GameObject)Instantiate(terrainTiles[terrain[x, y].tileType].tile, new Vector3(terrain[x, y].worldPosition.x, yValue, terrain[x, y].worldPosition.y), Quaternion.identity);
                newTile.transform.parent = islandMap.transform;
                terrain[x, y].worldTile = newTile;
                terrain[x, y].areaName = "";
                newTile.GetComponent<MapViewTile>().mapTile = terrain[x, y];
                newTile.GetComponent<MapViewTile>().SetTileByMask();

                if (terrain[x,y].featureType != FeatureType.None)
                {
                    newFeature = (GameObject)Instantiate(featureTiles[terrain[x, y].featureType].tile, new Vector3(terrain[x, y].worldPosition.x, 1f, terrain[x, y].worldPosition.y), Quaternion.Euler(-90, 0, 0));
                    newFeature.transform.parent = newTile.transform;
                    terrain[x, y].featureTile = newFeature;
                    terrain[x, y].featureName = terrain[x, y].featureType.ToString() + " of " + NameGenerator.GetRandomName();
                }

            }
        }
    }

    void InstantiateMonsters()
    {
        monsterController.InstantiateMonsters(terrain);
    }

    void PutRoads()
    {
        foreach (MapTile townTile in featuresInMap[FeatureType.Town])
        {
            foreach (MapTile townTileEnd in featuresInMap[FeatureType.Town])
            {
                List<Vector2> newRoad = new List<Vector2>();
                newRoad.Add(townTileEnd.gridPosition);
                RoadBetween(townTile, townTileEnd, newRoad);
            }
            foreach (MapTile cityTile in featuresInMap[FeatureType.City])
            {
                List<Vector2> newRoad = new List<Vector2>();
                newRoad.Add(cityTile.gridPosition);
                RoadBetween(townTile, cityTile, newRoad);
            }
        }
    }

    void RoadBetween(MapTile startTile, MapTile endTile, List<Vector2> tilesInRoad)
    {
        if (startTile.gridPosition == endTile.gridPosition)
        {
            //The same tile
            SetRoadConnections(tilesInRoad);
            return;
        } else if (roadConnections[(int)startTile.gridPosition.x, (int)startTile.gridPosition.y, (int)endTile.gridPosition.x, (int)endTile.gridPosition.y])
        {
            //already connected
            SetRoadConnections(tilesInRoad);
            return;
        } else
        {
            int mod = (int)startTile.gridPosition.x % 2;
            int x1 = (int)startTile.gridPosition.x;
            int y1 = (int)startTile.gridPosition.y;
            int x2 = (int)endTile.gridPosition.x;
            int y2 = (int)endTile.gridPosition.y;
            int endX, endY;
            int roadAngle;

            if (x2 > x1)
            {
                endX = x1 + 1;

                if (y2 > y1)
                {
                    endY = y1 + 1 - mod;
                    roadAngle = 60;
                } else 
                {
                    endY = y1 - mod;
                    roadAngle = 120;
                }
            } else if (x2 == x1)
            {
                endX = x1;

                if (y2 > y1)
                {
                    endY = y1 + 1;
                    roadAngle = 0;
                }
                else if (y2 == y1)
                {
                    //Impossible, but if both are equal then it ends
                    SetRoadConnections(tilesInRoad);
                    return;
                }
                else
                {
                    endY = y1 - 1;
                    roadAngle = 180;
                }
            } else
            {
                endX = x1 - 1;

                if (y2 > y1)
                {
                    endY = y1 + 1 - mod;
                    roadAngle = 300;
                }
                else 
                {
                    endY = y1 - mod;
                    roadAngle = 240;
                }
            }

            PlotRoadBetween(x1, y1, endX, endY, roadAngle);
            tilesInRoad.Add(new Vector2(x1, y1));
            RoadBetween(terrain[endX, endY], endTile, tilesInRoad);
        }
    }

    void PlotRoadBetween(int x1, int y1, int x2, int y2, int angle)
    {
        GameObject newRoadSegment1 = (GameObject)Instantiate(roadSegment, new Vector3(terrain[x1, y1].worldPosition.x, .4f, terrain[x1, y1].worldPosition.y), Quaternion.identity);
        newRoadSegment1.transform.Rotate(0, -angle,0);
        newRoadSegment1.transform.parent = roadContainer.transform;
        GameObject newRoadSegment2 = (GameObject)Instantiate(roadSegment, new Vector3(terrain[x2, y2].worldPosition.x, .4f, terrain[x2, y2].worldPosition.y), Quaternion.identity);
        newRoadSegment2.transform.Rotate(0, 180-angle,0);
        newRoadSegment2.transform.parent = roadContainer.transform;
    }

    void SetRoadConnections(List<Vector2> roadTiles)
    {
        foreach (Vector2 v1 in roadTiles)
        {
            foreach (Vector2 v2 in roadTiles)
            {
                roadConnections[(int)v1.x, (int)v1.y, (int)v2.x, (int)v2.y] = true; //TODO deberia contener las conexiones tile a tile, no nodo a nodo
            }
        }
    }

    void NameTerrainAreas()
    {
        TileType tileType;
        bool named;

        for (int x = 0; x < terrainWidth; x++)
        {
            for (int y = 0; y < terrainHeight; y++)
            {
                named = (terrain[x, y].areaName != "");

                tileType = terrain[x, y].tileType;

                if (!named)
                {
                    string areaName = terrain[x, y].tileType.ToString() + " of " + NameGenerator.GetRandomName();
                    GameObject newMapArea = (GameObject)Instantiate(mapArea, new Vector3(terrain[x, y].worldPosition.x, 0, terrain[x, y].worldPosition.y), Quaternion.identity);
                    newMapArea.GetComponent<MapArea>().SetAreaName(areaName);
                    newMapArea.GetComponent<MapArea>().tileType = tileType;
                    newMapArea.transform.SetParent(mapAreasContainer.transform);
                    NameMapArea(newMapArea.GetComponent<MapArea>(), x, y, tileType, areaName);
                }
            }
        }
    }

    void NameMapArea(MapArea mapArea, int x, int y, TileType tileType,string areaName)
    {
        mapArea.GetComponent<MapArea>().addAreaTile(terrain[x, y]);
        areaNames.Add(mapArea.gameObject);
        terrain[x, y].mapArea = mapArea.GetComponent<MapArea>();

        terrain[x, y].areaName = areaName;

        for (int i = x - 1; i <= x+1; i++)
        {
            for (int j = y - 1; j <= y+1; j++)
            {
                if ((i >= 0) && (j >= 0) && (i < terrainWidth) && (j < terrainHeight))
                {
                    if ((terrain[i,j].tileType == tileType) && (terrain[i, j].areaName == ""))
                    {
                        NameMapArea(mapArea, i, j, tileType, areaName);
                    }
                }
            }
        }
    }

    //Returns a random type of terrain tile (TileType)
    TileType GetRandomTerrainTile()
    {
        //TODO improve random selection, not depending on chances in every tile data
        float randomResult = Random.Range(1, 100);
        float total = 0;
        for (int i = 0; i < tileData.Length; i++)
        {
            total += tileData[i].chance;
            if (randomResult <= total)
                return tileData[i].tileType;
        }
        return tileData[0].tileType;
    }

    int GetSurroundingTiles(int x, int y, TileType tileType)
    {
        int count = 0;
        int m = x % 2;

        if (CheckTypeOfTile(x, y + 1, tileType))
            count++;

        if (CheckTypeOfTile(x + 1, y + 1 - m, tileType))
            count++;

        if (CheckTypeOfTile(x + 1, y - m, tileType))
            count++;

        if (CheckTypeOfTile(x, y - 1, tileType))
            count++;

        if (CheckTypeOfTile(x - 1, y - m, tileType))
            count++;

        if (CheckTypeOfTile(x - 1, y + 1 - m, tileType))
            count++;

        return count;
    }

    bool CheckTypeOfTile(int x, int y, TileType tileType)
    {
        if ((x < 0) || (y < 0) || (x >= terrainWidth) || (y >= terrainHeight))
            return false;

        if (terrain[x, y].tileType == tileType)
            return true;
        else
            return false;
    }

    public MapTile FindRandomFeature(FeatureType featureType)
    {
        MapTile[] tilesWithFeature = featuresInMap[featureType].ToArray();

        return tilesWithFeature[Random.Range(0, tilesWithFeature.Length - 1)];
    }
}
