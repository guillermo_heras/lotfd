﻿using UnityEngine;
using System.Collections;

public class CharacterPawn : MonoBehaviour {

    private bool _baseVisible;
    public MeshRenderer[] meshRenderers;

	// Use this for initialization
	void Start () {
        _baseVisible = true;
        meshRenderers = gameObject.GetComponentsInChildren<MeshRenderer>();
    }

    public void SetBaseVisible(bool visible)
    {
        _baseVisible = visible;
        SetMeshRenderersActive(visible);
    }

    private void SetMeshRenderersActive(bool active)
    {
        foreach (MeshRenderer meshRenderer in meshRenderers)
        {
            meshRenderer.enabled = active;
        }
    }
	
}
