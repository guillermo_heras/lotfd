﻿using UnityEngine;
using System.Collections;

public class NameGenerator  {

    private static string[] baseSyllables = {"a","e","i","o","u","qa","qe","qi","qia","qea","quu","war","dik","llaal","nu","ni","na","po","ep","tra","-","'","ax","urx","ivi","ehr","fa","ej" };

    public static string GetRandomName()
    {
        int length = Random.Range(1, 10);
        string name = "";

        for (int count = 0; count < length; count++)
        {
            name += baseSyllables[Random.Range(0, baseSyllables.Length - 1)];
            count++;
        }

        return name;
    }
}
