﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonsterInMap
{
    public GameObject monster;

    public int x, y;

    public MonsterInMap(GameObject monster, int x, int y)
    {
        this.monster = monster;
        this.x = x;
        this.y = y;
    }
}

public class MonsterController : MonoBehaviour {

    public GameObject[] monsterCollection;

    private List<MonsterInMap> monsterPositions;

    void Awake()
    {
        monsterPositions = new List<MonsterInMap>();
    }

    public GameObject getRandomMonster()
    {
        return monsterCollection[Random.Range(0, monsterCollection.Length - 1)];
    }

    public void AddMonsterToMap(GameObject monster, int x, int y)
    {
        monsterPositions.Add(new MonsterInMap(monster, x, y));
    }

    public void InstantiateMonsters(MapTile[,] terrain)
    {
        GameObject newMonster;

        foreach (MonsterInMap monsterInMap in monsterPositions)
        {
            newMonster = (GameObject)Instantiate(monsterInMap.monster, new Vector3(terrain[monsterInMap.x, monsterInMap.y].worldPosition.x, 0, terrain[monsterInMap.x, monsterInMap.y].worldPosition.y), Quaternion.identity);
            newMonster.transform.parent = gameObject.transform;
            newMonster.GetComponent<CharacterPawn>().SetBaseVisible(false);
        }
    }

}
