﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(TimeControl))]
public class TimeControlEditor : Editor
{

    public override void OnInspectorGUI()
    {
        TimeControl myTimeControl = (TimeControl)target;

        DrawDefaultInspector();

        if (GUILayout.Button("+1 hour"))
        {
            myTimeControl.TimeJump(1);
        }

        if (GUILayout.Button("+3 hours"))
        {
            myTimeControl.TimeJump(3);
        }

        if (GUILayout.Button("+8 hours"))
        {
            myTimeControl.TimeJump(8);
        }

        if (GUILayout.Button("+1 day"))
        {
            myTimeControl.TimeJump(0, 1);
        }

        if (GUILayout.Button("+10 days"))
        {
            myTimeControl.TimeJump(0, 10);
        }

    }


}
