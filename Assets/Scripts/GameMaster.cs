﻿using UnityEngine;
using System.Collections;

public class GameMaster : MonoBehaviour {

    private PlayerCharacter pc;
    private Camera playerCamera;

    private TimeControl timeControl;

    private IslandGenerator islandGenerator;

	// Use this for initialization
	void Start () {
        pc = GameObject.FindObjectOfType<PlayerCharacter>();
        islandGenerator = GameObject.FindObjectOfType<IslandGenerator>();
        playerCamera = GameObject.FindObjectOfType<Camera>();
        timeControl = GameObject.FindObjectOfType<TimeControl>();

        islandGenerator.GenerateTerrain();

        PlaceHero();
    }


    
    public void PlaceHero()
    {
        MapTile initialTown = islandGenerator.FindRandomFeature(FeatureType.Town);

        pc.gameObject.transform.position = new Vector3(initialTown.worldPosition.x, pc.gameObject.transform.position.y, initialTown.worldPosition.y);
        playerCamera.transform.position = new Vector3(initialTown.worldPosition.x, playerCamera.transform.position.y, initialTown.worldPosition.y);
        playerCamera.transform.LookAt(pc.transform);
        pc.SetInitialPosition((int)initialTown.gridPosition.x, (int)initialTown.gridPosition.y);
    }


    public void TimePasses(float hours)
    {
        timeControl.TimeJump(hours);
    }


}
